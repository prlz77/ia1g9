from copy import deepcopy
from io import *
from alphabetProcessor import *
import sys

# Kind of weights suported for graph edges
WEIGHTS = [0,1,2,3,4]
# If set to true propts extra info
VERBOSE = False

# Creates a new matrix reordering the rows and column of the original one
# given as the firs argument, with the order given by the second list, the second argument
def reorderMatrix(matrix,order):
    reordered_matrix = []
    for pos in order:
        # It choose the row which it has to change
        row = matrix[pos]
        ordered_row = []
        # It choose the column which it has to change
        for pos2 in order:
            ordered_row.append(row[pos2])
        reordered_matrix.append(ordered_row)
    return reordered_matrix

# Creates an array with the indexes of the rows which have the same nodes as the original one
def getSameNode(matrix,original_row):
    similar_rows = []
    for row_index in range(len(matrix)):
        # If a row have the same edges of the original row, it adds the index of this node to the new array
        if sorted(matrix[row_index]) == sorted(original_row):
            similar_rows.append(row_index)
    return similar_rows


# Given two adjacency matrix returns if symmetric graphs are equivalent,
# if VERBOSE is set to true, it gives you more information
def match(adjacency1,adjacency2,fixed_row_list=[[],[]]):
    if VERBOSE:
        print "### ENTRA ###"
        print "fixed_row_list: " + str(fixed_row_list)
        print "adjacency1: " + str(adjacency1)
        print "adjacency2: " + str(adjacency2)
        print "#############"
    
    # If the reordered matrix has the same length and is equal to matrix1
    # return True but if they have the same length and are not equal return false
    if len(fixed_row_list[0]) == len(adjacency1):
        if reorderMatrix(adjacency2,fixed_row_list[1]) == reorderMatrix(adjacency1,fixed_row_list[0]):
            return True
        return False

    # To seek next row which shall be fixed
    next_to_fix = 0
    while next_to_fix in fixed_row_list[0]:
        next_to_fix += 1 
    
    if VERBOSE:
        print "NTF: " + str(next_to_fix)
    
    # Get all the rows which match with the current one
    possible_matches = getSameNode(adjacency2,adjacency1[next_to_fix])
    
    # Fix non repeated rows and call recursive match for each ambiguous case.
    for i in possible_matches:
        # If the row has already been fixed there's no need to do anything
        if i not in fixed_row_list[1]:
            # Deepcopy shall be used or object is the same for each execution
            cfixed_row_list = deepcopy(fixed_row_list) 
            cfixed_row_list[0].append(next_to_fix)
            cfixed_row_list[1].append(i)
            
            # Vector which has Nones to the rows that aren't fixed and the position where the rows are fixed
            to_fix_A = []
            to_fix_B = []
            
            for j in range(len(adjacency1[next_to_fix])):
                if j not in cfixed_row_list[0]:
                    to_fix_A.append(adjacency1[next_to_fix][j])
                else:
                    to_fix_A.append(None)
            for j in range(len(adjacency2[i])):
                if i not in cfixed_row_list[1]:
                    to_fix_B.append(adjacency2[i][j])
                else:
                    to_fix_B.append(None)
            
            # If there's only one element to fix of a determinate weight, we can fix it.
            for j in WEIGHTS:
                if to_fix_A.count(j)==1 and to_fix_A.index(j) not in cfixed_row_list[0] and j in to_fix_A and j in to_fix_B:
                    cfixed_row_list[0].append(to_fix_A.index(j))
                    cfixed_row_list[1].append(to_fix_B.index(j))
            
            if VERBOSE:
                print "to_fix_A: " + str(to_fix_A)
                print "fixed_row_list" +str(cfixed_row_list)
            
            # If match is achieved return True.
            if match(adjacency1,adjacency2,cfixed_row_list):
                return True
                
    return False

# Given an adjacency array, which is symmetric, and creates an array with
# the letters from the Alphabet which have the same nodes. All the edges are set to one.
def match_flat_edges(adjacency, alphabet):
    
    # Deepcopy shall be used or object is the same for each execution
    copy=deepcopy(adjacency)
    possibles=[]

    # Edges are represent with a 1
    for row in range(len(copy)):
        for column in range(row,len(copy)): #es simetrica
            if copy[row][column]!=0:
                copy[row][column]=1 
                copy[column][row]=1
    position=0
    
    # Compares with the flat alphabet, and appens the possible matches to a new vector
    for letter in alphabet.flatAlphabet:
        if match(copy,letter):
            possibles.append(alphabet.alphabet[position])
        position+=1
    return possibles


