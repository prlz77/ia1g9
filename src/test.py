from main import *
import time

class Test:
    # Inizialization
    def __init__(self, coded_text, original_text = None):
        self.arguments = sys.argv[1:]
        self.textRecognizer = TextRecognizer(coded_text)
        self.coded_text = coded_text
        self.original_text = ""
        # Calculates the number of letters from the original text
        if original_text != None:
            f = open(original_text, 'r')
            for i in f:
                self.original_text += i
            f.close()

    # Tests the graphMatching function without symmetrize
    def testMatch(self):
        print "Testing plain Match with: " + self.coded_text
        # Calculate the time of an execution in seconds
        start_time = time.time()
        # Executes the function
        uncoded_text = self.textRecognizer.graphMatching()
        print "Time expended: \t"
        print time.time() - start_time, "seconds"
        # Prints a report
        self.printReport(uncoded_text)
        print "OK."
                
    # Tests the graphMatching function with symmetrize
    def testMatchSimetrize(self):
        print "Testing Match and Simetrize with: " + self.coded_text
        # Calculate the time of an execution in seconds
        start_time = time.time()
        # Executes the function
        uncoded_text = self.textRecognizer.graphMatching(True)
        print "Time expended: \t"
        print time.time() - start_time, "seconds"
        # Prints a report
        self.printReport(uncoded_text)
        print "OK."
                
    # Tests the hashDecoder function without symmetrize
    def testHash(self):
        print "Testing plain Hash with: " + self.coded_text
        # Calculate the time of an execution in seconds
        start_time = time.time()
        # Executes the function
        uncoded_text = self.textRecognizer.hashDecoder()
        print "Time expended: \t"
        print time.time() - start_time, "seconds"
        # Prints a report
        self.printReport(uncoded_text)
        print "OK."
                
    # Tests the hashDecoder function with symmetrize
    def testHashSimetrize(self):
        print "Testing Hash and Simetrize with: " + self.coded_text
        # Calculate the time of an execution in seconds
        start_time = time.time()
        # Executes the function
        uncoded_text = self.textRecognizer.hashDecoder(True)
        print "Time expended: \t"
        print time.time() - start_time, "seconds"
        # Prints a report
        self.printReport(uncoded_text)
        print "OK."
                
    # Tests the match_with_hash function
    def testMatchWithHash(self):
        print "Testing Match with Hash with: " + self.coded_text
        # Calculate the time of an execution in seconds
        start_time = time.time()
        # Executes the function
        uncoded_text = self.textRecognizer.match_with_hash()
        print "Time expended: \t"
        print time.time() - start_time, "seconds"
        # Prints a report
        self.printReport(uncoded_text)
        print "OK."
                
    # Tests the match_double_hash function
    def testDoubleHash(self):
        print "Testing Match with Double Hash with: " + self.coded_text
        # Calculate the time of an execution in seconds
        start_time = time.time()
        # Executes the function
        uncoded_text = self.textRecognizer.match_double_hash()
        print "Time expended: \t"
        print time.time() - start_time, "seconds"
        # Prints a report
        self.printReport(uncoded_text)
        print "OK."
                
    # Tests the plainMatchLocalDistances function
    def testLocalDistances(self):
        print "Testing Match with Local Distances with: " + self.coded_text
        # Calculate the time of an execution in seconds
        start_time = time.time()
        # Executes the function
        uncoded_text = self.textRecognizer.plainMatchLocalDistances()
        print "Time expended: \t"
        print time.time() - start_time, "seconds"
        # Prints a report
        self.printReport(uncoded_text)
        print "OK."
                
    # Tests all the functions
    def testAll(self):
        self.testMatch()
        self.testMatchSimetrize()
        self.testHash()
        self.testHashSimetrize()
        self.testMatchWithHash()
        self.testDoubleHash()
        self.testLocalDistances()
    
    # Normalize a text given. It erase from the text the special characters like '\n', '\r' and ' '
    def normalize(self,text):
        returntext = ""
        for i in text:
            if i != '\n' and i != '\r' and i != ' ':
                returntext += i
        return returntext
                
    # Prints a report with the information of an execution (if an optional variable is set) and then returns some of this information
    def printReport(self, uncoded_text,information=True):
        if self.original_text == "":
            return None
        
        # Normalizes the two texts to compare
        textA = self.normalize(self.original_text)
        textB = self.normalize(uncoded_text)
        successes = 0
        misses = 0
        blank = 0
        total = 0
        # Compares the two texts
        for i in range(min(len(textA),len(textB))):
            # If they are equal
            if textA[i] == textB[i]:
                successes += 1
            # A blank letter
            elif textB[i] == '-':
                blank += 1
                misses += 1
            # Not equal letters
            else:
                misses +=1
            
            total += 1
        
        # Information of the test, if information variable is set
        if information:
            print "==== REPORT ===="
            print "Total of characters"
            print total
            print "Number of successes"
            print successes
            print "Number of misses"
            print str(misses) + " from which " + str(blank) + " are in blank"
            print "Success rate"
            print str(100*float(successes)/float(total)) + "%"
            print "Miss rate"
            print str(100*float(misses)/float(total)) + "%"
                
        
        return(successes,misses,blank,total)

# python test.py
# Shows a help message with the use of test.py        
def show_help():
    print "Usage:"
    print "python test.py test_number coded_text_path [original_text_path]"
    print "Test Number:"
    print "\t0: All"
    print "\t1: match"
    print "\t2: simetrize and match"
    print "\t3: hash"
    print "\t4: simetrize and hash\n"
    print "\t5: match with hash\n"
    print "\t6: match with double hash\n"
    print "Coded text path: the path for the coded text\n"
    print "Original text path: the path for the original text. It will generate"
    print "a report\n"

    

if __name__== "__main__":
    argv = sys.argv[1:]
    test = None
    # Takes the arguments from the command line
    if len(argv) == 2:
        option = argv[0]
        test = Test(argv[1])
    elif len(argv) == 3:
        option = argv[0]
        test = Test(argv[1], argv[2])
    else:
        show_help()
        exit()
        
    if option == '0':
        test.testAll()
    elif option == '1':
        test.testMatch()
    elif option == '2':
        test.testMatchSimetrize()
    elif option == '3':
        test.testHash()
    elif option == '4':
        test.testHashSimetrize()
    elif option == '5':
        test.testMatchWithHash()
    elif option == '6':
        test.testDoubleHash()
    elif option == '7':
        test.testLocalDistances()
        

