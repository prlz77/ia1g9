Testing plain Match with: ../data/text_6.txt
Time expended: 	
0.287621974945 seconds
==== REPORT ====
Total of characters
901
Number of successes
105
Number of misses
796 from which 135 are in blank
Success rate
11.653718091%
Miss rate
88.346281909%
OK.
Testing Match and Simetrize with: ../data/text_6.txt
Time expended: 	
0.355769872665 seconds
==== REPORT ====
Total of characters
901
Number of successes
105
Number of misses
796 from which 135 are in blank
Success rate
11.653718091%
Miss rate
88.346281909%
OK.
Testing plain Hash with: ../data/text_6.txt
Time expended: 	
0.0668029785156 seconds
==== REPORT ====
Total of characters
906
Number of successes
543
Number of misses
363 from which 0 are in blank
Success rate
59.9337748344%
Miss rate
40.0662251656%
OK.
Testing Hash and Simetrize with: ../data/text_6.txt
Time expended: 	
0.0834679603577 seconds
==== REPORT ====
Total of characters
906
Number of successes
543
Number of misses
363 from which 0 are in blank
Success rate
59.9337748344%
Miss rate
40.0662251656%
OK.
Testing Match with Hash with: ../data/text_6.txt
Time expended: 	
0.355744123459 seconds
==== REPORT ====
Total of characters
906
Number of successes
543
Number of misses
363 from which 0 are in blank
Success rate
59.9337748344%
Miss rate
40.0662251656%
OK.
Testing Match with Double Hash with: ../data/text_6.txt
Time expended: 	
1.41910505295 seconds
==== REPORT ====
Total of characters
906
Number of successes
553
Number of misses
353 from which 0 are in blank
Success rate
61.0375275938%
Miss rate
38.9624724062%
OK.
