from copy import deepcopy
from io import *
from match import *
from hashMatcher import *
from alphabetProcessor import *
WEIGHTS = [0,1,2,3,4]

def getSameNode(matrix,original_row):
    similar_rows = []
    for row_index in range(len(matrix)):
        if sorted(matrix[row_index]) == sorted(original_row):
            similar_rows.append(row_index)
    return similar_rows

def getSubmatrix(matrix,order):
    reordered_matrix = []
    for pos in order:
        row = matrix[pos]
        ordered_row = []
        for pos2 in order:
            ordered_row.append(row[pos2])
        reordered_matrix.append(ordered_row)
    return reordered_matrix


def distance(adjacency1,adjacency2,fixed_row_list=[[],[]], submatrix=[]):
    if len(fixed_row_list[0]) == len(adjacency1):
        print "1"
        print fixed_row_list
        return fixed_row_list
    next_to_fix = 0
    while next_to_fix in fixed_row_list[0]:
        next_to_fix += 1 
    possible_matches = getSameNode(adjacency2,adjacency1[next_to_fix])
    print possible_matches
    for i in possible_matches:
    # If the row has already been fixed there's no need to do anything
           if i not in fixed_row_list[1]:
                cfixed_row_list = deepcopy(fixed_row_list) # Deepcopy shall be used or object is the same for each execution
                cfixed_row_list[0].append(next_to_fix)
                cfixed_row_list[1].append(i)
                to_fix_A = [] # Vector which has Nones to the rows that aren't fixed and the position where the rows are fixed
                to_fix_B = []
                for j in range(len(adjacency1[next_to_fix])):
                    if j not in cfixed_row_list[0]:
                        to_fix_A.append(adjacency1[next_to_fix][j])
                    else:
                        to_fix_A.append(None)
                for j in range(len(adjacency2[i])):
                    if i not in cfixed_row_list[1]:
                        to_fix_B.append(adjacency2[i][j])
                    else:
                        to_fix_B.append(None)
                print "to_fix"
                print to_fix_A
                print to_fix_B
                for j in WEIGHTS:
                    if to_fix_A.count(j)==1 and to_fix_A.index(j) not in cfixed_row_list[0] and j in to_fix_A and j in to_fix_B:
                        cfixed_row_list[0].append(to_fix_A.index(j))
                        cfixed_row_list[1].append(to_fix_B.index(j))
                    print "cfixed"
                    print cfixed_row_list
                    if getSubmatrix(adjacency1,cfixed_row_list[0])==getSubmatrix(adjacency2, cfixed_row_list[1]):
                        recursivity_fixed_row_list =  distance(adjacency1,adjacency2,cfixed_row_list)
                        print "recursivity"
                        print recursivity_fixed_row_list
                        if len(getSubmatrix(adjacency1,cfixed_row_list[0]))> len( recursivity_fixed_row_list):
           ##if len(getSubmatrix(adjacency1,cfixed_row_list[0]))>len(getSubmatrix(adjacency1,fixed_row_list[0])):
                            print "2"
                            print cfixed_row_list
                            return cfixed_row_list
                        else:
                            print "3"
                            print recursivity_fixed_row_list
                            return recursivity_fixed_row_list
                    else:
                        return []
    print "4"
    print fixed_row_list
    return fixed_row_list
'''
def search(adjacency1,adjacency2,possible):
    submatrix=[]
    for i in range(len(adjacency2)):
        for j in range(i,len(adjacency2)):
            for k in range(len(possible)):
                if adjacency[i][j] in possible:
                    submatrix.append([possible,(i,j)])
                if len(possible)==len(submatrix):
                    


def permute(list1,row):
    b=[]
    for x in range(len(list1)):
        if x not in row:
            a=deepcopy(row)
            a.append(x)
            b.append(a)
    return b
        
def getDegree(node):
    degree = 0
    for i in node:
        if i != 0:
            degree +=1
    return degree

def getSubmatrix(matrix,order):
    reordered_matrix = []
    for pos in order:
        row = matrix[pos]
        ordered_row = []
        for pos2 in order:
            ordered_row.append(row[pos2])
        reordered_matrix.append(ordered_row)
    return reordered_matrix


def dist(adjacency1, adjacency2, to_fix=0, fixed_list=[[],[]]):
    if fixed_list[0]==[]:
        possible = Permute(adjacency1[to_fix],[to_fix])
        for x in possible:
            submatrix = Serch(adjacency1,adjacency2,x)
            if getSubmatrix(adjacency1,x)==getSubmatrix(adjacency2, ):
'''

def match_flat_edges(adjacency):
    copy=deepcopy(adjacency)
    possibles=[]
    fileReader = FileReader("../data/alfabet_codificat.txt")
    alph=fileReader.readPlainAlphabet("../data/alfabet.txt")
    allMatrix = fileReader.readAllMatrixFromCodedAlphabet()
    for row in range(len(copy)):
        for column in range(len(copy)):
            if copy[row][column]!=0:
                copy[row][column]=1 
    allMatrix1=deepcopy(allMatrix)
    position=0
    for letter in allMatrix1:
        for row in range(len(letter)):
            for column in range(len(letter)):
                if letter[row][column]!=0:
                    letter[row][column]=1
        if match(copy,letter):
            possibles.append((alph[position],allMatrix[position]))
        position+=1
    return possibles

            
            


def hash_edges(matrix, list):
    hash_matrix=0
    hash_list=0
    closer_letter=None
    
    fileReader = FileReader("../data/alfabet_codificat.txt")
    allMatrix = fileReader.readAllMatrixFromCodedAlphabet()
    list_dic=[]
    for letter, matrix1 in list:
        list_dic.append(matrix1)
    alph=AlphabetProcessor(list_dic)
    weight=alph.getWeightFrequency().keys()
    #dic={}
    #for i in weight:
    #    dic[i]=11**i
    dic={0:0,1:11,2:29,3:47,4:63}
    for row in range(len(matrix)):
        for column in range(len(matrix)):
            if matrix[row][column] in dic:
                hash_matrix+=dic[matrix[row][column]]
            else:
                hash_matrix+=dic[2] 
    hash_matrix=float(hash_matrix)#/float((len(matrix)**2))
    print "matrix"
    print hash_matrix
    delta=None
    position=0
    current_pos=0
    for letter, matrix1 in list:
        for row in range(len(matrix1)):
            for column in range(len(matrix1)):
                hash_list+=dic[matrix1[row][column]]
        hash_list=float(hash_list)#/float(len(matrix1)**2)
        #       print "list"
        #       print hash_list
        #       print letter
        print "---"
        print hash_list
        print hash_matrix
        print abs(hash_list-hash_matrix)
        print "---"
        if position==0:
            delta=abs(hash_list-hash_matrix)+1
        if (abs(hash_list-hash_matrix)<delta):
            delta=abs(hash_list-hash_matrix)
            #print delta
            closer_letter = letter
            current_pos=position
            print "pos"
            print current_pos
        hash_list=0
        position+=1
    print list
    print position
    i=0
    for i in range(len(allMatrix)):
        if allMatrix[i]==list[current_pos]:
            print "hola"
            print allMatrix[i]
            print current_pos
            break
    print delta
    #print closer_letter
    return (delta,i)
     


if __name__ == "__main__":
    matrix =[[ 0, 4, 0, 1],[ 0, 0, 5, 3],[ 0, 5, 0, 0],[ 1, 3, 0, 0]  ] 
#N
    '''4 Crec que es una N
        [ 0, 4, 0, 1]   0 1 0 0
        [ 0, 0, 5, 3]   1 0 3 0
        [ 0, 5, 0, 0]   0 3 0 1
        [ 1, 3, 0, 0]   0 0 1 0
        '''
    A=[[0,2,3],[4,0,2],[3,2,0]]
    B=[[0,5,7],[3,0,1],[3,2,0]]
    fileReader1 = FileReader("../data/alfabet_codificat.txt")
    fileReader2 = FileReader("../data/text_4.txt")
    
    allMatrix = fileReader1.readAllMatrixFromCodedAlphabet()
    fileReader1.generateCorrespondenceDict("../data/alfabet.txt")
    a=simetrize(matrix, [])
    for i in a:
        print "-----------"
        llista = match_flat_edges(i)
        print llista
        delta, current=hash_edges(matrix, llista)
        print delta, current
        print allMatrix[current]
        print "-----------"




