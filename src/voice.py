import subprocess
import os

class Speaker:
    # Inizialization
    def __init__(self, text):
        self.text = text
        # Checks if the system has installed festival
        if not self.checkFestival():
            print "Oops! It seems that you haven't installed festival on your system"
            print "If you want text to speech functions, please install it"
            exit()
    
    # Checks if the system has installed festival
    def checkFestival(self):
        if os.path.exists("/usr/bin/festival") or os.path.exists("/bin/festival"):
            return True
        else:
            return False
    # Stores the text in a temporal file to read it if it's a long text
    def saveFile(self):
        os.system("rm -rf tmp")
        os.system("mkdir tmp")
        f = open("tmp/tts.tmp",'w')
        for i in self.text:
            f.write(i)
        f.close()

    # Process the text erasing the special characters
    def processText(self,text):
        text2 = ""
        for i in range(len(text)):
            if text[i] != '\n' and text[i] != '\r':
                text2 += text[i]
            elif text[i-1] != " " and i < len(text) and text[i+1] != " ":
                text2 += " "
    def setText(self,text):
        self.text = self.processText(text)

    # Gets the text
    def getText(self):
        return self.text

    # Calls festival with the text
    def textToSpeech(self):
        subprocess.call("festival --tts " + "tmp/tts.tmp", shell=True)
