from alphabetProcessor import *
from match import *
from io import *

# Calculates de fegree of a node
def getDegree(node):
    degree = 0
    for i in node:
        if i != 0:
            degree +=1
    return degree


class Hasher:
    # Inizialization
    def __init__(self,alphabet, nodefreqForce = 2.0, degreeForce = 2.5, weightForce = 0.5):
        self.nodefreqForce = nodefreqForce
        self.degreeForce = degreeForce
        self.weightForce = weightForce
        self.alphabet = alphabet
        self.hashedAlphabet = []
        self.alphabetProcessor = AlphabetProcessor(self.alphabet)
        for i in self.alphabet:
            self.hashedAlphabet.append(self.hashMatrix(i))
    
    # Calculates a number that expresses the characteristics
    # of a matrix based on the most and least frequent traits of the alphabet
    # characters.
    def hashMatrix(self,matrix):
        hash_number = 0
        # Gets the statistics of alphabet processor about the number of nodes
        nodefreq = self.alphabetProcessor.getNodeCountFrequency()
        
        # Calculates the hash function depending on the number of nodes
        if nodefreq.has_key(len(matrix)):
            hash_number += len(matrix) * (1 - nodefreq[len(matrix)]) * self.nodefreqForce
        else:
            hash_number += 1
        
        # Gets the statistics of alphabet processor about the weight of the edges
        weights = self.alphabetProcessor.getWeightFrequency()
        # Gets the statistics of alphabet processor about the degree of nodes
        degrees = self.alphabetProcessor.getDegreeCountFrequency()
                
        for node in matrix:
            degree = getDegree(node)
            # Calculates the hash function depending on the degree of nodes
            if degrees.has_key(degree):
                hash_number += degree * (1 - degrees[degree]) * self.degreeForce
            else:
                hash_number += 1
            # Calculates the hash function depending on the weight of the edges
            for weight in node:
                if weights.has_key(weight):
                    hash_number += weight * (1 - weights[weight]) * self.weightForce
                else:
                    hash_number += 1
       
        return hash_number
    
    # Gets the nearest matrix in the alphabet from the matrix in the input
    def getNearestMatrix(self,matrix):
        # Calculates the hash of the input matrix
        hashedMatrix = self.hashMatrix(matrix)
        currentIndex = 0
        currentDelta = abs(self.hashedAlphabet[0] - hashedMatrix)
        # Look for the nearest matrix in the alphabet
        for i in range(len(self.hashedAlphabet)):
            if abs(self.hashedAlphabet[i] - hashedMatrix) < currentDelta:
                currentIndex = i
                currentDelta = abs(self.hashedAlphabet[i] - hashedMatrix)
        # Returns de difference between the hash functions and the index in the alphabet
        return (currentDelta, currentIndex)

    # Gets the nearest matrix in the alphabet from an array of matrix in the input
    def getNearestMatrixInArray(self, matrixArray):

        deltas = []
        # Calculates the hash function of all the matrix in the Array
        for matrix in matrixArray:
            deltas.append(self.getNearestMatrix(matrix))
        
        mindelta = deltas[0][0]
        nearestIndex = 0
        # Calculates the smallest delta
        for i in range(len(deltas)):
            if deltas[i][0] < mindelta:
                mindelta = deltas[i][0]
                nearestIndex = i
        
        return deltas[nearestIndex]
    
    # Gets the nearest matrix in the alphabet from the matrix in the input using a different hash function
    def getNearestMatrix_deltaControl(self,matrix):
        # Calculates the possible list of matching using the function match_flat_edges
        possible_list=match_flat_edges(matrix, self.alphabetProcessor)
        deltas=self.hash_edges(matrix, possible_list)
        # If the possible_list is empty, we use the first has function
        if deltas[0]>=0:
            return deltas
        else:
            return self.getNearestMatrix(matrix)

    # Gets the nearest matrix in the alphabet from an array of matrix in the input using a different hash function
    def getNearestMatrixInArray_deltaControl(self, matrixArray):
        deltas = []
        # Calculates the hash function of all the matrix in the array
        for matrix in matrixArray:
            deltas.append(self.getNearestMatrix_deltaControl(matrix))
        
        mindelta = deltas[0][0]
        nearestIndex = 0
        # Calculates the smallest delta
        for i in range(len(deltas)):
            if deltas[i][0] < mindelta:
                mindelta = deltas[i][0]
                nearestIndex = i
        
        return deltas[nearestIndex]

    # Calculates a hash function based on the weights of the edges and compares it with a list of possible matches
    def hash_edges(self,matrix, list_possible):
        hash_matrix=0
        hash_list=0

        # Dictionary to set a very distant value, when it calculates the hash function
        dic={0:0,1:11,2:29,3:47,4:63}
        
        # Hash function for the input matrix
        for row in range(len(matrix)):
            for column in range(len(matrix)):
                if matrix[row][column] in dic:
                    hash_matrix+=dic[matrix[row][column]]
                else:
                    hash_matrix+=dic[2] 
        delta=None
        position=0
        current_pos=0
        # For all the matrix in the possible list, it calculates their hash function
        for matrix1 in list_possible:
            for row in range(len(matrix1)):
                for column in range(len(matrix1)):
                    hash_list+=dic[matrix1[row][column]]
            if position==0:
                delta=abs(hash_list-hash_matrix)+1
            # Stores the smallest delta
            if (abs(hash_list-hash_matrix)<delta):
                delta=abs(hash_list-hash_matrix)
                current_pos=position
            hash_list=0
            position+=1
        # If no possible matches, return (-1,0) to know the error
        if list_possible==[]:
            return (-1, 0)
        # Look for the index of the letter in the alphabet
        for i in range(len(self.alphabetProcessor.alphabet)):
            if self.alphabetProcessor.alphabet[i]==list_possible[current_pos]:
                break
        return (delta,i)

    # A distance between nodes
    def distanceBetweenNodes(self,node1, node2):
        distance = 0
        # First adds the difference between their degree
        distance += abs(getDegree(node1) - getDegree(node2))
        found = []
        for i in node1:
            if i not in found:
                # Adds the difference between the different types of edges
                distance += abs(node1.count(i) - node2.count(i))
                found.append(i)
        return distance
    
    # A distance between two matrix using the distance between nodes
    def localDistances(self, adjacency1, adjacency2):
        mindistances = 0
        distances = []
        for node1 in adjacency1:
            for node2 in adjacency2:
                # Calculates the distance between one node from the first input matrix to all the nodes in the second input matrix
                distances.append(self.distanceBetweenNodes(node1,node2))
            # Adds to the distance the smaller distance between the nodes
            mindistances += min(distances)
            distances = []
        return mindistances


