from copy import *

# It calculates the Degree of a node
def getDegree(node):
    degree = 0
    for i in node:
        if i != 0:
            degree +=1
    return degree

# A class which has different methods to apply into the alphabet
class AlphabetProcessor:
    
    # Inizialization
    def __init__(self,alphabet):
        self.alphabet = alphabet # Codified alphabet
        self.weightFrequency = self.genWeightFrequency() # A dictionary with the statistics of the different weights of the edges
        self.nodeCountFrequency = self.genNodeCountFrequency() # A dictionary with the frequency of the different number of the nodes in the differents letters of the alphabet
        self.degreeCountFrequency = self.genDegreeCountFrequency() # A dictionary with the statistics of the different weights of the nodes
        self.flatAlphabet=self.genFlatAlphabet(alphabet) # Codified alphabet where all the edges have weight 1
    
    # Functions to get the different frequencies
    def getWeightFrequency(self):
        return self.weightFrequency
    def getNodeCountFrequency(self):
        return self.nodeCountFrequency
    def getDegreeCountFrequency(self):
        return self.degreeCountFrequency
    def getFlatAlphabet(self):
        return self.flatAlphabet
    
    # Function to set the alphabet
    def setAlphabet(self,alphabet):
        self.alphabet = alphabet
    
    # Generates a dictionary with the statistics of the different weights of the edges
    def genWeightFrequency(self):
        weights = {}
        total = 0
        for matrix in self.alphabet:
            for row in matrix:
                # Calculates the number of edges with the different weight and stores it in a dictionary
                for weight in row:
                    if weights.has_key(weight):
                        weights[weight] += 1
                    else:
                        weights[weight] = 1
                    total += 1
        
        for w in weights:
            # Divides the different counts of weight with the total number of elements
            weights[w] = float(weights[w]) / float(total)
            
        return weights
    
    # Generates a dictionary with the frequency of the different number of the nodes in the differents letters of the alphabet
    def genNodeCountFrequency(self):
        nodes = {}
        total = 0
        # Calculates the number of different nodes
        for matrix in self.alphabet:
            if nodes.has_key(len(matrix)):
                nodes[len(matrix)] += 1
            else:
                nodes[len(matrix)] = 1
            total += 1
        
        for n in nodes:
            # Divides the different counts of nodes with the total
            nodes[n] = float(nodes[n]) / float(total)
        return nodes
    
    # Generates a dictionary with the statistics of the different weights of the nodes
    def genDegreeCountFrequency(self):
        degrees = {}
        total = 0
        # Calculates the number of different degrees of the nodes
        for matrix in self.alphabet:
            for node in matrix:
                if degrees.has_key(getDegree(node)):
                    degrees[getDegree(node)] += 1
                else:
                    degrees[getDegree(node)] = 1
                total += 1
        
        for d in degrees:
            # Divides the different counts of degrees with the total
            degrees[d] = float(degrees[d]) / float(total)
        return degrees
    
    # Generates an alphabet where all the edges have weight 1
    def genFlatAlphabet(self,Matrix):
        flatAlphabet=[]
        allMatrix=deepcopy(Matrix)
        for letter in allMatrix:
            for row in range(len(letter)):
                for column in range(row,len(letter)):
                    if letter[row][column]!=0:
                        letter[row][column]=1
                        letter[column][row]=1
            flatAlphabet.append(letter)
        return flatAlphabet








