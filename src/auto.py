from main import *
from test import *

# Calculates what ara the best parameters for the Hash function, using the class test
class Auto:
    # Inizialization
    def __init__(self, coded_text,test):
        self.textRecognizer = TextRecognizer(coded_text)
        self.coded_text = coded_text
        self.test=test
    
    # Function hashDecoder without symmetrize
    def autoHash(self):
        nodefreq=1.6
        stadistics=(0,0,0,None)
        results=(0,0,0,0)
        while nodefreq<2.6:
            # Sets de NodefreqForce
            self.textRecognizer.setNodefreqForce(nodefreq)
            degree=2
            while degree<3:
                # Sets de DegreeForce
                self.textRecognizer.setDegreeForce(degree)
                weight=0
                while weight<1:
                    # Sets de WeightForce
                    self.textRecognizer.setWeightForce(weight)
                    uncoded_text = self.textRecognizer.hashDecoder()
                    # Stores the results of the test
                    results1=self.test.printReport(uncoded_text,False)
                    # Stores the best results and the necessary stadistics
                    if results1[0]>results[0]:
                        results=results1
                        stadistics=(nodefreq,degree,weight)
                    weight+=0.2
                degree+=0.2
            nodefreq+=0.2
        # Prints a report
        print "Succes Rate:\t"+str(100*float(results[0])/float(results[3])) + "%"
        print "Stadistics:\t"+str(stadistics)
        return stadistics

    # Function hashDecoder with symmetrize
    def autoHashSimetrize(self):
        nodefreq=1.6
        stadistics=(0,0,0,None)
        results=(0,0,0,0)
        while nodefreq<2.6:
            # Sets de NodefreqForce
            self.textRecognizer.setNodefreqForce(nodefreq)
            degree=2
            while degree<3:
                # Sets de DegreeForce
                self.textRecognizer.setDegreeForce(degree)
                weight=0
                while weight<1:
                    # Sets de WeightForce
                    self.textRecognizer.setWeightForce(weight)
                    uncoded_text = self.textRecognizer.hashDecoder(True)
                    # Stores the results of the test
                    results1=self.test.printReport(uncoded_text,False)
                    # Stores the best results and the necessary stadistics
                    if results1[0]>results[0]:
                        results=results1
                        stadistics=(nodefreq,degree,weight)
                    weight+=0.2
                degree+=0.2
            nodefreq+=0.2
        # Prints a report
        print "Succes Rate:\t"+str(100*float(results[0])/float(results[3])) + "%"
        print "Stadistics:\t"+str(stadistics)
        return stadistics

    # Function match_double_hash
    def autoDoubleHash(self):
        nodefreq=1.6
        stadistics=(0,0,0,0)
        results=(0,0,0,0)
        while nodefreq<2.6:
            # Sets de NodefreqForce
            self.textRecognizer.setNodefreqForce(nodefreq)
            degree=2
            while degree<3:
                # Sets de DegreeForce
                self.textRecognizer.setDegreeForce(degree)
                weight=0
                while weight<1:
                    # Sets de WeightForce
                    self.textRecognizer.setWeightForce(weight)
                    uncoded_text = self.textRecognizer.match_double_hash()
                    # Stores the results of the test
                    results1=self.test.printReport(uncoded_text,False)
                    # Stores the best results and the necessary stadistics
                    if results1[0]>results[0]:
                        results=results1
                        stadistics=(nodefreq,degree,weight)
                    weight+=0.2
                degree+=0.2
            nodefreq+=0.2
        # Prints a report
        print "Succes Rate:\t"+str(100*float(results[0])/float(results[3])) + "%"
        print "Stadistics:\t"+str(stadistics)
        return stadistics

    # All the functions
    def autoAll(self):
        print "---Hash---"
        self.autoHash()
        print "---Hash Simetrize---"
        self.autoHashSimetrize()
        print "---Double Hash---"
        self.autoDoubleHash()


# python auto.py
# Shows a help message with the use of auto.py    
def show_help():
    print "Usage:"
    print "python auto.py test_number coded_text_path original_text_path"
    print "Test Number:"
    print "\t0: Hash"
    print "\t1: Hash simetrize"
    print "\t2: Double Hash"
    print "\t3: All"
    print "Coded text path: the path for the coded text\n"
    print "Original text path: the path for the original text. It will generate"
    print "a report\n"


if __name__== "__main__":
    # Takes the arguments from the command line
    argv = sys.argv[1:]
    test = None
    if len(argv) == 2:
        option = argv[0]
        test = Test(argv[1])
    elif len(argv) == 3:
        option = argv[0]
        test = Test(argv[1], argv[2])
    else:
        show_help()
        exit()

    auto=Auto(argv[1],test)

    if option == '0':
        auto.autoHash()
    elif option == '1':
        auto.autoHashSimetrize()
    elif option == '2':
        auto.autoDoubleHash()
    elif option == '3':
        auto.autoAll()

