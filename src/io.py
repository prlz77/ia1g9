from copy import deepcopy

# Kind of weights suported for graph edges
WEIGHTS = [0,1,2,3,4]

# Prints a matrix
def printMatrix(matArray):
    buffer = "\n"
    for i in matArray:
        for j in i:
            buffer += str(j) + " "
        buffer += "\n"
    print buffer

# Create an array with the changes to make a matrix symetric
def isSymmetric(matrix):
    no_symmetric_pos=[]
    for i in range(len(matrix)):
        for j in range(i,len(matrix)):
            # If the matrix isn't symmetric, the program adds the changes it needs to make the matrix symmetric
            if matrix[i][j]!=matrix[j][i]:
                no_symmetric_pos.append((i,j))
    return no_symmetric_pos

# Makes the matrix symetric
# To call this function: simetrice(matrix,[])
def simetrize(matrix, all):
    # If the matrix have an edge with a weight not in the normal ones, it normalizes it
    # by trying all the possible combinations or only change with the diagonal one
    for row in range(len(matrix)):
        for column in range(len(matrix)):
            if  matrix[row][column]>4:
                if matrix[column][row] in WEIGHTS:
                    matrix[row][column]=matrix[column][row]
                else:
                    for i in WEIGHTS:
                        # Deepcopy shall be used or object is the same for each execution
                        cmatrix=deepcopy(matrix)
                        cmatrix[row][column]=i
                        cmatrix[column][row]=i
                        return simetrize(cmatrix,all)
    # Tries if the matrix is symmetric
    sim=isSymmetric(matrix)
    # Symmetric management
    if sim==[]:
        all.append(matrix)
        return all
    else:
        # If the matrix isn't symmetric, it changes between the different combinations of the diagonal
        a,b=sim.pop()
        # Deepcopy shall be used or object is the same for each execution
        cmatrix=deepcopy(matrix)
        cmatrix[a][b]=matrix[b][a]
        A=simetrize(cmatrix,all)
        # Deepcopy shall be used or object is the same for each execution
        cmatrix1=deepcopy(matrix)
        cmatrix1[b][a]=matrix[a][b]
        return simetrize(cmatrix1,A)
    
# FileReader let you read different aspects of an input text
class FileReader:
    # Inizialization
    def __init__(self, coded_text):
        self.coded_text = coded_text
        self.coded_text_fd = open(coded_text, 'r')
            
        self.correspondence_dict = {}
        
        self.plain = []
        
        self.simetrize = simetrize
    
    # Puts the coded_text_fd to the beginning of the file
    def reloadFiles(self):
        self.coded_text_fd.seek(0)
    
    # Generates a dictionary between the uncoded_text and the different lines of the file
    # Is is usually called with the alphabet, to generate a dictionary between the letter and their matrix
    def generateCorrespondenceDict(self, uncoded_text):
        uncoded_text_fd = open(uncoded_text,'r')
        self.reloadFiles()
        self.correspondence_dict = {}
        for i in uncoded_text_fd.readlines():
            self.correspondence_dict[i[0]] = self.readMatrixFromFile()
        uncoded_text_fd.close()
    
    # Returns the dictionary of the file
    def getCorrespondenceDict(self):
        return self.correspondence_dict

    # Stores an uncoded_text into an array, it is used to store de alphabet uncoded
    def readPlainAlphabet(self,uncoded_text):
        uncoded_text_fd = open(uncoded_text,'r')
        self.plain = []
        for i in uncoded_text_fd.readlines():
            self.plain.append(i[0])
        uncoded_text_fd.close()
        return self.plain
       
    # Returns the alphabet in characters
    def getPlainAlphabet(self):
        return self.plain
    
    # Stores all the coded alphaben into an array of matrix
    def readAllMatrixFromCodedAlphabet(self):
        self.reloadFiles()
        codedMatrices = []
        
        current_matrix = self.readMatrixFromFile()
        while current_matrix != None:
            # Stores all the matrix of the alphabet in a list
            codedMatrices.append(current_matrix)
            current_matrix = self.readMatrixFromFile()
        
        return codedMatrices
    
    # Read a matrix from a file and stores it in an arrayof arrays
    def readMatrixFromFile(self):  
        # The matrix to return
        matrix = []
        
        # Get a line from the file
        file_line = self.coded_text_fd.readline()
        
        # End of file
        if file_line == "":
            return None
        
        # Handle special characters
        if len(file_line) <= 4:
            index = 0
            buffer = ""
            while index < len(file_line) and file_line[index] != '\n' and file_line[index] != '\r':
                buffer += file_line[index]
                index += 1
                
            if index == 0:
                return [['\n']]
            
            return [[int(buffer)]]
        
        # Parse line    
        line=map(int,file_line.split())
        
        # Get matrix dimensions
        matrix_order = line.pop(0)
        
        #Create array from file
        for row in range(matrix_order):
            row_to_add = []
            for column in range(matrix_order):
                row_to_add.append(line[row * matrix_order + column])
            matrix.append(row_to_add)
        return matrix 
    
    # Read a matrix from a file and symmetrize it
    def readSymmetricMatrixFromFile(self):
        matrix = self.readMatrixFromFile()
        if matrix == None:
            return None
        # Special char management
        elif len(matrix) <= 2:
            return matrix
        else:
            return simetrize(matrix, [])

