# -*- coding: utf8 -*- 
''' 
__TITLE__
Text Recognizer

__DESCRIPTION__
Main class of text recognition program. It uses graph matching and distance algorithms
to find the closest match for a handwritten letter.

__AUTHORS__
Pau Rodríguez López 1243264, Pau Riba Fierrez 1244936, Roger Pastor Ortiz  1242137, Marc Riera Casals 1242263"

__VERSION__
1.10

__DATE__
Last edited on 18/11/2012
'''

from io import *
from match import *
from hashMatcher import *
from voice import *

class TextRecognizer:
    
    def __init__(self, codified_text, alphabet="../data/alfabet.txt", codified_alphabet = "../data/alfabet_codificat.txt", nodefreqForce = 2.0, degreeForce = 2.5, weightForce = 0.5):
        self.alphabet = alphabet
        self.codified_alphabet = codified_alphabet
        self.codified_text = codified_text
        self.nodefreqForce = nodefreqForce
        self.degreeForce = degreeForce
        self.weightForce = weightForce
    
    # Hash parameters:
    # Sets de value nodefreqForce.
    def setNodefreqForce(self,nodefreqForce):
        self.nodefreqForce = nodefreqForce
    # Sets de value degreeForce.
    def setDegreeForce(self,degreeForce):
        self.degreeForce = degreeForce
    # Sets de value weightForce.
    def setWeightForce(self,weightForce):
        self.weightForce = weightForce
    
    # Graph matching function. If simetrize is set to true, matrices are simetrized before matched
    def graphMatching(self, simetrize = False):
        # Prepare to read matrices from files
        alphabetReader = FileReader(self.codified_alphabet)
        textReader = FileReader(self.codified_text)
        
        # Select proper read function
        if simetrize:
            readText = textReader.readSymmetricMatrixFromFile
        else:
            readText = textReader.readMatrixFromFile
        
        # Get all the alphabet matrices
        alphabet = alphabetReader.readAllMatrixFromCodedAlphabet()
        # Get character - matrix correspondence in alphabet
        alphabetReader.generateCorrespondenceDict(self.alphabet)
        
        # Get a row text matrix from file
        received_text = readText()
        # String where the output is stored
        text_to_show = ""

        # While not end of file
        while received_text != None:
            # Indicates whether a match has been done
            found = False

            # Special char management
            if len(received_text[0]) > 2:
                # When we don't simetrize we only receive one matrix
                if not simetrize:
                    received_text = [received_text]

                # Try to match each matrix with all the alphabet
                for text_matrix in received_text:
                    for character_matrix in alphabet:
                        if len(character_matrix) == len(text_matrix) and match(text_matrix,character_matrix):
                            # Look for the letter in de CorrespondenceDict
                            for character, matrix in alphabetReader.getCorrespondenceDict().iteritems():
                                if matrix == character_matrix:
                                    text_to_show += character
                            found = True
                            break
                    if found:
                        break
            else:
                found = True
                text_to_show += self.__getSpecialChars(received_text[0][0])
            # If the letter is not found, we mark it with '-'
            if not found:
                text_to_show += '-'

            received_text = readText()

        return text_to_show

    # Hash decoder function. If simetrize is set to true, matrices are simetrized before matched
    # It uses the hash function to decide what letter is it.
    def hashDecoder(self, symmetrize = False):
        
        # Prepare to read matrices from files
        alphabetReader = FileReader(self.codified_alphabet)
        textReader = FileReader(self.codified_text)
        
        # Select proper read function
        if symmetrize:
            readText = textReader.readSymmetricMatrixFromFile
        else:
            readText = textReader.readMatrixFromFile
        
        # Get all the alphabet matrices
        alphabet = alphabetReader.readAllMatrixFromCodedAlphabet()
        # Get character - matrix correspondence in alphabet
        alphabetReader.generateCorrespondenceDict(self.alphabet)
        
        plain_alphabet = alphabetReader.readPlainAlphabet(self.alphabet)
        
        # Get a raw text matrix from file
        received_text = readText()
        # String where the output is stored
        text_to_show = ""
       
        # A new Hasher with the parameters.
        hasher = Hasher(alphabet, self.nodefreqForce, self.degreeForce, self.weightForce)
    
        # Handle symmetrizing
        if symmetrize:
            # While not end of file
            while received_text != None: 
                # Special char management
                if len(received_text[0]) > 2:
                    text_to_show += plain_alphabet[hasher.getNearestMatrixInArray(received_text)[1]]
                else:
                    text_to_show += self.__getSpecialChars(received_text[0][0])
                    
                received_text = readText()
        else:
            # While not end of file
            while received_text != None:
                # Special char management
                if len(received_text) > 2:
                    text_to_show += plain_alphabet[hasher.getNearestMatrix(received_text)[1]]
                else:
                    text_to_show += self.__getSpecialChars(received_text[0][0])        
                            
                received_text = readText() 
            

        return text_to_show
    
    # Local Distances function. Always symmetrize the input matrix
    # Uses a Local Distances function between the nodes
    def plainMatchLocalDistances(self):
        
        # Prepare to read matrices from files
        alphabetReader = FileReader(self.codified_alphabet)
        fileReader = FileReader(self.codified_text)
        
        plain_alphabet = alphabetReader.readPlainAlphabet(self.alphabet)
        # Get all the alphabet matrices
        allMatrix = alphabetReader.readAllMatrixFromCodedAlphabet()
        
        # Get character - matrix correspondence in alphabet
        alphabetReader.generateCorrespondenceDict(self.alphabet)
        received_text = fileReader.readMatrixFromFile()
        
        alphabet=AlphabetProcessor(allMatrix)
        
        # A new Hasher with the parameters.
        hasher = Hasher(allMatrix, self.nodefreqForce, self.degreeForce, self.weightForce)
        
        text_to_show = ""
        
        # While not end of file
        while received_text != None:
            distance =0 
            current_matrix = None
            mindistance = None
            found = False
            # Special char management
            if len(received_text) > 2:
                simetrized_matrices = simetrize(received_text,[])
                for simetrized_matrix in simetrized_matrices:
                    possible_matches = match_flat_edges(simetrized_matrix, alphabet)

                    for text_matrix in possible_matches:
                        distance=hasher.localDistances(text_matrix,simetrized_matrix)

                        if distance<mindistance or mindistance==None:
                            mindistance=distance
                            current_matrix = text_matrix
                # If there's no possible_matches with 
                if current_matrix == None:
                    current_matrix = hasher.getNearestMatrixInArray(simetrized_matrices)[1]
                    text_to_show += plain_alphabet[current_matrix]
                else:
                    # Try to match each matrix with all the alphabet
                    for character, matrix in alphabetReader.getCorrespondenceDict().iteritems():
                        if matrix == current_matrix:
                            text_to_show += character
            else:
                text_to_show += self.__getSpecialChars(received_text[0][0])
        
            received_text = fileReader.readMatrixFromFile() 

        return text_to_show
    
    # For every letter match_with_hash tries to apply GraphMatching but if this method
    # doesn't decide what letter is it, a hash function is applied.
    def match_with_hash(self):
        
        # Prepare to read matrices from files
        alphabetReader = FileReader(self.codified_alphabet)
        fileReader = FileReader(self.codified_text)

        plain_alphabet = alphabetReader.readPlainAlphabet(self.alphabet)
        # Get all the alphabet matrices
        alphabet = alphabetReader.readAllMatrixFromCodedAlphabet()
        # Generates de correspondance dictionary
        alphabetReader.generateCorrespondenceDict(self.alphabet)
        matrix2 = fileReader.readMatrixFromFile()
        text_to_show = ""
        # A new Hasher with the parameters.
        hasher = Hasher(alphabet, self.nodefreqForce, self.degreeForce, self.weightForce)
        # While not end of file
        while matrix2 != None:
            found = False
            simetrized_matrix2 = simetrize(matrix2,[])
            if len(matrix2) > 2:
                # GraphMatching method
                for matrix3 in simetrized_matrix2:
                    for matrix1 in alphabet:
                        if len(matrix1) == len(matrix3) and match(matrix1,matrix3):
                            # Look for the letter in de CorrespondenceDict
                            for character, matrix in alphabetReader.getCorrespondenceDict().iteritems():
                                if matrix == matrix1:
                                    text_to_show += character
                            found = True
                            break
                    if found:
                        break
            else:
                text_to_show += self.__getSpecialChars(matrix2[0][0])
            # If the GraphMatching methon doesn't decide. A hasher is applied
            if not found and len(matrix2) > 2:
                text_to_show += plain_alphabet[hasher.getNearestMatrixInArray(simetrize(matrix2,[]))[1]]

            matrix2 = fileReader.readMatrixFromFile() 

        return text_to_show
            
    # If a hasher function doesn't decide what letter is it, an another hash function is applied.
    def match_double_hash(self):
        
        # Prepare to read matrices from files
        alphabetReader = FileReader(self.codified_alphabet)
        fileReader = FileReader(self.codified_text)
        
        plain_alphabet = alphabetReader.readPlainAlphabet(self.alphabet)
        
        # Get all the alphabet matrices
        alphabet = alphabetReader.readAllMatrixFromCodedAlphabet()
        # Generates de correspondance dictionary
        alphabetReader.generateCorrespondenceDict(self.alphabet)
        matrix2 = fileReader.readMatrixFromFile()
        text_to_show = ""
        # A new Hasher with the parameters.
        hasher = Hasher(alphabet, self.nodefreqForce, self.degreeForce, self.weightForce)
        # While not end of file
        while matrix2 != None:
            # Special char management
            if len(matrix2) > 2:
                text_to_show += plain_alphabet[hasher.getNearestMatrixInArray_deltaControl(simetrize(matrix2,[]))[1]]
            else:
                text_to_show += self.__getSpecialChars(matrix2[0][0])
            
            matrix2 = fileReader.readMatrixFromFile() 
        
        return text_to_show

    # Special keys like spaces, comas, dots...
    def __getSpecialChars(self, value):
        if value == 51:
            return  " "
        elif value == 52:
            return  ","
        elif value == 53:
            return  "."
        elif value == '\n':
            return  "\n"
        else:
            return  "?"

# python main.py
# Shows a help message with the use of main.py
def show_help():
    print "Usage:"
    print "python main.py coded_text_path [speaker]"
    print "Coded text path: the path for the coded text\n"
    print "Speaker:\n"
    print "\t0: Text to Speech desactivated\n"
    print "\t1: Text to Speech activated\n"


if __name__ == "__main__":
    argv = sys.argv[1:]
    option = '0'
    if len(argv) == 1:
        text_recognizer = TextRecognizer(argv[0])
    elif len(argv) == 2:
        option = argv[1]
        text_recognizer = TextRecognizer(argv[0])
    else:
        show_help()
        exit()
    
    #print text_recognizer.graphMatching()
    #print text_recognizer.graphMatching(True)
    #print text_recognizer.hashDecoder()
    #print text_recognizer.hashDecoder(True)
    #print text_recognizer.match_with_hash()
    uncoded_text = text_recognizer.match_double_hash()
    print uncoded_text

    #print text_recognizer.plainMatchLocalDistances()
    if(option == '1'):
        speaker = Speaker(uncoded_text)
        speaker.saveFile()
        speaker.textToSpeech()
    
